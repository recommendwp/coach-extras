<?php
/*
Plugin Name: Coach Template Extras
Description: Add some custom functions to Coach Template theme.
Version: 1.0
Author: RecommendWP
Author URI: http://www.recommendwp.com
Bitbucket Plugin URI: https://bitbucket.org/webdevsuperfast/coach-extras
License: GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.txt
*/

defined( 'ABSPATH' ) or die( esc_html_e( 'With great power comes great responsibility.', 'coach-extras' ) );

class Coach_Extras {
    function __construct() {
        // Custom PHP
        // require_once( plugin_dir_path( __FILE__ ) . '/inc/custom.php' );

        // Scripts & Styles
        // add_action( 'wp_enqueue_scripts', array( $this, 'cte_enqueue_scripts' ) );

        // Example template
        // add_filter( 'siteorigin_widgets_form_options_rwpw-image', array( $this, 'cte_extend_image_form' ), 10, 2 );

        // add_filter( 'siteorigin_widgets_template_file_rwpw-image', array( $this, 'cte_extend_image_template' ), 10, 3 );
    }

    public function cte_enqueue_scripts() {
        if ( !is_admin() ) {
            // Custom CSS
            wp_enqueue_style( 'cte-css', plugin_dir_url( __FILE__ ) . 'css/custom.css' );

            // Custom JS
            wp_register_script( 'cte-js', plugin_dir_url( __FILE__ ) . 'js/custom.js', array( 'jquery' ), null, true );
            wp_enqueue_script( 'cte-js' );
        }
    }

    public function cte_extend_image_form( $form_options, $widget ) {
        if ( !empty( $form_options['template']['options'] ) ) {
            $form_options['template']['options']['image'] = __( 'Custom Image', 'coach-extras' );
        }

        return $form_options;
    }

    public function cte_extend_image_template( $filename, $instance, $widget ) {
        if ( !empty( $instance['template'] ) && $instance['template'] == 'image' ) {
            $filename = plugin_dir_path( __FILE__ ) . '/inc/templates/image.php';
        } 

        return $filename;
    }
}

new Coach_Extras();